const app = require('express')()

app.get('/', function (req, res) {
  res.sendFile((__dirname + '/index.html'), function(err) {
      if(err) {
          console.log(err)
      } else {
          console.log("Sent file: ", 'index')
      }
  })
})

app.listen(8000, function () {
    console.log('listening on *:8000');
})